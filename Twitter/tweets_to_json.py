#!/usr/bin/env python
# encoding: utf-8

# fork by https://gist.github.com/ppcfish/634a2cce0c1bd926f6adc6d9bd6e628b
# original by https://gist.github.com/yanofsky/5436496
# support python3 and export chinese
import tweepy #https://github.com/tweepy/tweepy
import sys
import os
import io
import json
import time
import linecache
import logging

#import threading
from concurrent import futures
from operator import itemgetter

settings = json.load(open('settings.json', 'r'))

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(threadName)s %(thread)d [line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

#Twitter API credentials
consumer_key = settings['consumer_key']
consumer_secret = settings['consumer_secret']
access_token = settings['access_token']
access_token_secret = settings['access_token_secret']

#authorize twitter, initialize tweepy
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, timeout=30)


def get_all_tweets(screen_name):
    #Twitter only allows access to a users most recent 3240 tweets with this method

    thread_startime = time.clock()
    logging.info("geting %s 's tweets" % screen_name)

    #initialize a list to hold all the tweepy Tweets
    alltweets = []  
    
    #make initial request for most recent tweets (200 is the maximum allowed count)
    new_tweets = api.user_timeline(screen_name = screen_name, count = 200)
    
    #save most recent tweets
    alltweets.extend(new_tweets)
    logging.info('...%s tweets downloaded so far' % (len(alltweets)))

    #save the id of the oldest tweet less one
    oldest = alltweets[-1].id - 1 if len(alltweets) > 0 else 0

    #load tweet file and initialize since_id
    benchmark = 100
    isdelete = 0
    maxid = 12345
    if os.path.exists('%s_tweets.json' % screen_name) and os.path.getsize('%s_tweets.json' % screen_name):
        #exist_tweet = linecache.getlines('%s_tweets.json' % screen_name)
        #exist_tweet_json = [json.loads(line.rstrip('\n')) for line in exist_tweet]
        exist_tweet_json = [line.rstrip('\n') for line in linecache.getlines('%s_tweets.json' % screen_name)]
        exist_tweet = [json.loads(line) for line in exist_tweet_json]
  
        #when some tweets delete in timeline, Fetch still get 3240 tweets limit, but more history, so marge exist data
        if new_tweets[0].user.statuses_count < exist_tweet[benchmark]['user']['statuses_count'] :
            isdelete = 1
            logging.warning('...unfortunately some tweets was delete, need get all timeline to marge!')    
        elif len(exist_tweet) > benchmark :        #overwrite last rows
            maxid = exist_tweet[benchmark]['id']   #exist_tweet[0]['id']
  
    #keep grabbing tweets until there are no tweets left to grab
    while len(new_tweets) > 0 and oldest > maxid:
        logging.info('getting tweets before %s' % (oldest))
        
        #all subsiquent requests use the max_id param to prevent duplicates
        new_tweets = api.user_timeline(screen_name = screen_name, count = 200, since_id= maxid, max_id = oldest)
                
        #save most recent tweets
        alltweets.extend(new_tweets)
        
        #update the id of the oldest tweet less one
        oldest = alltweets[-1].id - 1
        
        logging.info('...%s tweets downloaded so far' % (len(alltweets)))
    
    #transform the tweepy tweets into a 2D array that will populate the csv
    #outtweets = [[tweet.id_str, tweet.created_at, tweet.text.encode('utf-8')]
    alltweets_json = [json.dumps(tweet._json, ensure_ascii=False)  for tweet in alltweets]

    if os.path.exists('%s_tweets.json' % screen_name) and os.path.getsize('%s_tweets.json' % screen_name):
    #if len(exist_tweet):
        
        #exist_tweet_json = [json.loads(line.rstrip('\n')) for line in exist_tweet]
        #exist_tweet_id = json.loads(line)['id']

        wait_marge_tweets = []
        exist_tweet_id = [tweet['id'] for tweet in exist_tweet]
        alltweets_id = [tweet.id for tweet in alltweets]
        
        #print (exist_tweet[i]['id'])
        if exist_tweet[0]['id'] < oldest + 1: 
            wait_save_tweets = alltweets_json + exist_tweet_json
            logging.warning('get data does not link old data, some tweets maybe lost')             

        elif (oldest + 1 in exist_tweet_id) and (isdelete != 1) :
            # exist_tweet   slice    download     marge       lost
            # [8,7,6,5,4]   [5,4]    [9,8,6,5]   [9,8,6,5,4]  [7]

            beginrow = exist_tweet_id.index(oldest + 1)
            wait_save_tweets = alltweets_json + exist_tweet_json[beginrow:]

            logging.info('data slice and marge!') 
            
        else:
            for index, item in enumerate(exist_tweet_id) : 
                if item not in alltweets_id:   
                    alltweets_json.append(exist_tweet_json[index])     

            wait_marge_tweets = [json.loads(line.rstrip('\n')) for line in alltweets_json]
            sorted_list = sorted(wait_marge_tweets, key=itemgetter('id'), reverse=True)
            wait_save_tweets = [json.dumps(tweet, ensure_ascii=False) for tweet in sorted_list]

            logging.info('download tweet has being marge existing data!') 
        
        #wait_save_tweets.extend([json.dumps(tweet, ensure_ascii=False) for tweet in exist_tweet[beginrow:]])
        #wait_save_tweets.extend([tweet.rstrip('\n') for tweet in exist_tweet[beginrow:]])
        #print (wait_save_tweets)
    else:
        wait_save_tweets = alltweets_json

    #print ('\nWriting to file: %s\n' % filename)
    with open('%s_tweets.json' % screen_name, 'w', encoding='utf-8') as f:
        temp1 = f.write('\n'.join(wait_save_tweets))
        #temp1 = f.write(str(wait_save_tweets))
        #temp1 = f.writeline('\n'.join(wait_save_tweets))
        #f.write('\n')
    linecache.clearcache()

    logging.info("...geting %s 's tweets used %f seconed" % (screen_name, time.clock() - thread_startime))
    exit()

if __name__ == '__main__':
    # pass in the username of the account you want to download
    
    start_time = time.clock()
    '''
    myscreen_name = 'myname'
    max_thread = 10
    i = 0
    
    # creat thread pool with ProcessPoolExecutor or ThreadPoolExecutor
    with futures.ThreadPoolExecutor(max_workers = max_thread) as executor:   
        for status in api.friends_ids(screen_name = myscreen_name, count = 2000):  #max 2000
            
            if i > 0 :
                # get friends screen_name
                friends_screen_name = api.get_user(user_id = str(status)).screen_name

                # submit thread
                test1 = executor.submit(get_all_tweets, friends_screen_name)
            i = i + 1
    
    logging.info('geting all data used %f seconed' % (time.clock() - start_time))

    '''
    userlist = ['jayrkevin123']
    # userlist = ['weareliveinhere']
    for users in userlist:
        print("geting %s 's tweets" % users)
        get_all_tweets(users)
    