# WeAreLiveInHere Analysis
Strange (ARG?) I found while crawling through Twitter. Upon further inspection, seems like this is a kid, from the UK? I'm gonna stop looking into it now as I can't tell if it's an ARG, an "art project" by a kid or if he's in danger. Also using youtube-dl config from [here](https://github.com/dmn001/youtube_channel_archiver).
**INSTRUCTIONS ARE NOT COMPLETE**

## Contributing
Please please please, share all links and modifications which could make this project better. Also, whoever made this, if you're reading, and you genuinely are that little kid. Keep doing what you're doing, you've got something special.

## SoundCloud
Use download_archive.bat to archive this [SoundCloud](https://soundcloud.com/weareliveinhere) which is related to wearelive. If new accounts are found, add them to youtube-dl-channels.txt

Liked by this account was this 'track': https://soundcloud.com/user-577277918/fcartzg0extb

## YouTube
Use download_archive.bat to archive these channels [1](https://www.youtube.com/channel/UCmW-3ahMZcy8YqVEiyAsEDQ) and [2](https://www.youtube.com/channel/UCCypMHO5jk1n2cJizz-MJvg) If new accounts are found, add them to youtube-dl-channels.txt

## Twitter
**CAN'T FIND BETTER WAY TO DUMP EVERYTHING**
Use provided scripts, not made by me , the creator links are at top of scripts.
Twitter accounts of interest so far are [jayrkevin123](https://twitter.com/jayrkevin123/), and [weareliveinhere](https://twitter.com/weareliveinhere)

## Website?

The [website](https://jayrkevinsite.neocities.org/) was found on the first Twitter account linked above. Use wget to recursively archive the site.

[A blog too?](https://jkevinsblog.blogspot.com/2019/)

[Old website?](https://jayrkevinsite.weebly.com/)

["Database Website" from jayrkevinsite.neocities](https://sekret.neocities.org/)
["Other Website" from jayrkevinsite.neocities](https://kevincctv.neocities.org/)
[Interesting.](https://jayrkevinsite.neocities.org/pages.html)

## Other pages of interest

### Streamable
There are multiple Streamable links spread across the communication channels.

### Vimeo?
https://vimeo.com/kevinpen and https://vimeo.com/weareliveinhere

### Periscope?
https://www.periscope.tv/Jayrkevin1
https://www.periscope.tv/wearelive

### Twitch
https://www.twitch.tv/nicekevinb1x
https://www.twitch.tv/weareliveinhere

### Dlive
https://dlive.tv/KevinInc

### Guestbook?
http://www.freegb.net/gbook/jayrkevinsite

### Reddit
https://www.reddit.com/user/Iamonvinyl/

### Mixer
https://mixer.com/harmoniccat8092
